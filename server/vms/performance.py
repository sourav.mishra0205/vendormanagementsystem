import logging
import datetime
from django.core.exceptions import ObjectDoesNotExist
from .serializers import PurchaseOrderSerializer, VendorPerformanceSerializer
from .models import PurchaseOrderBean, VendorPerformanceBean


def calculate_on_time_delivery_rate(vendor_id: str, completed_on_time: bool = True):
    
    if not vendor_id:
        return

    completed_orders = None
    serialized_completed_orders = []
    performance_serialized_data = {}

    try:
        completed_orders = PurchaseOrderBean.objects.filter(status='completed', vendor_id=vendor_id)
        for order in completed_orders:
            serialized_completed_orders.append(PurchaseOrderSerializer(order).data)
    except ObjectDoesNotExist:
        logging.info(f"No completed purchase orders found for the vendor with ID: {vendor_id}.")

    try:
        vendor_performance = VendorPerformanceBean.objects.get(vendor_id=vendor_id)
        performance_serialized_data = VendorPerformanceSerializer(vendor_performance).data
    except ObjectDoesNotExist:
        logging.info(f"Not Found - Performance of the vendor with ID: {vendor_id}.")
    
    total_completed_orders = len(completed_orders)
    updated_delivery_rate = (total_completed_orders * performance_serialized_data.get("on_time_delivery_rate", 0)
                             + int(completed_on_time)) / (total_completed_orders + 1)
    
    return updated_delivery_rate


def calculate_quality_rating_avg(vendor_id: str, quality_rating):

    if not vendor_id:
        return

    completed_orders = None
    serialized_completed_orders = []
    performance_serialized_data = {}

    try:
        completed_orders = PurchaseOrderBean.objects.filter(status='completed', vendor_id=vendor_id)
        for order in completed_orders:
            serialized_completed_orders.append(PurchaseOrderSerializer(order).data)
    except ObjectDoesNotExist:
        logging.info(f"No completed purchase orders found for the vendor with ID: {vendor_id}.")

    try:
        vendor_performance = VendorPerformanceBean.objects.get(vendor_id=vendor_id)
        performance_serialized_data = VendorPerformanceSerializer(vendor_performance).data
    except ObjectDoesNotExist:
        logging.info(f"Not Found - vendor with ID: {vendor_id}.")
    
    if not performance_serialized_data.get("quality_rating_avg", None):
        if quality_rating:
            return quality_rating
        return
    
    if not quality_rating:
        return performance_serialized_data.get("quality_rating_avg")
    
    total_completed_orders = len(completed_orders)
    updated_quality_rating = (total_completed_orders * performance_serialized_data.get("quality_rating_avg")
                              + quality_rating) / (total_completed_orders + 1)
    
    return updated_quality_rating
    

def calculate_average_response_time(purchase_order_request, origin_data):
    
    total_acknowledged = 0
    po_serialized_data = []
    performance_serialized_data = {}

    try:
        purchase_orders = PurchaseOrderBean.objects.filter(vendor_id=origin_data["vendor"])
        for order in purchase_orders:
            po_serialized_data.append(PurchaseOrderSerializer(order).data)
    except ObjectDoesNotExist:
        logging.info(f"No Orders Available.")

    for order in po_serialized_data:
        if order.get("acknowledgement_date", None):
            total_acknowledged += 1

    try:
        vendor_performance = VendorPerformanceBean.objects.filter(vendor_id=origin_data["vendor"])
        if vendor_performance:
            performance_serialized_data = VendorPerformanceSerializer(vendor_performance[0]).data
    except ObjectDoesNotExist:
        logging.info(f"Not Found - Performance of the vendor with ID: { origin_data['vendor'] }.")
    
    issue_date = origin_data.get("issue_date")
    ack_date = purchase_order_request.get("acknowledgement_date")
    
    issue_date_timestamp = datetime.datetime.strptime(issue_date, "%Y-%m-%dT%H:%M:%SZ").timestamp()\
        if type(issue_date) == str else issue_date.timestamp()
    ack_date_timestamp = datetime.datetime.strptime(ack_date, "%Y-%m-%dT%H:%M:%S.%fZ").timestamp()\
        if type(ack_date) == str else ack_date.timestamp()

    response_time = abs(issue_date_timestamp - ack_date_timestamp)
    updated_avg_response = (total_acknowledged * performance_serialized_data.get("average_response_time", 0)
                            + response_time) / (1 + total_acknowledged)
    
    return updated_avg_response


def calculate_fulfillment_rate(vendor_id, completed: bool):
    po_serialized_data = []
    performance_serialized_data = {}
    purchase_orders = None

    try:
        purchase_orders = PurchaseOrderBean.objects.filter(vendor_id=vendor_id)
        for order in purchase_orders:
            po_serialized_data.append(PurchaseOrderSerializer(order).data)
    except ObjectDoesNotExist:
        logging.info(f"No Orders Available.")
    
    try:
        vendor_performance = VendorPerformanceBean.objects.filter(vendor_id=vendor_id)
        if vendor_performance:
            performance_serialized_data = VendorPerformanceSerializer(vendor_performance[0]).data
    except ObjectDoesNotExist:
        logging.info(f"Not Found - performance of vendor with ID: {vendor_id}.")

    fulfilled_order = 0
    for order in po_serialized_data:
        if order.get("status") == 'Completed':
            fulfilled_order += 1
    total_orders = len(purchase_orders)
    updated_fulfillment_rate = (total_orders * performance_serialized_data.get("fulfillment_rate", 0)
                                + int(completed)) / total_orders
    
    return updated_fulfillment_rate

    
