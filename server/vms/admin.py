from django.contrib import admin
from .models import VendorBean, PurchaseOrderBean, VendorPerformanceBean
# Register your models here.

admin.site.register([VendorBean, PurchaseOrderBean, VendorPerformanceBean])