from django.urls import path
from .api.vendor_api import VendorAPI, VendorDetailsAPI
from .api.vendor_performance_api import VendorPerformanceAPI
from .auth import CustomAuthToken
from .api.purchase_order_api import PurchaseOrderAPI, PurchaseOrderDetailsAPI, PurchaseOrderAcknowledgeAPI

urlpatterns = [
    path('auth/', CustomAuthToken.as_view(), name="auth"),
    path('vendors/', VendorAPI.as_view(), name="vendors"),
    path('vendors/<vendor_id>/', VendorDetailsAPI.as_view(), name="vendor_details"),
    path('vendors/<vendor_id>/performance/', VendorPerformanceAPI.as_view(), name="vendor_performance"),
    
    path('purchase_orders/', PurchaseOrderAPI.as_view(), name="purchase_orders"),
    path('purchase_orders/<po_id>/', PurchaseOrderDetailsAPI.as_view(), name="purchase_order_details"),
    path('purchase_orders/<po_id>/acknowledge/', PurchaseOrderAcknowledgeAPI.as_view(), name="order_acknowledge"),
    


]