import datetime
from django.dispatch import receiver
from .models import PurchaseOrderBean, VendorPerformanceBean, VendorBean
from rest_framework.response import Response
from rest_framework import status
from .serializers import VendorPerformanceSerializer
from .api.purchase_order_api import PurchaseOrderDetailsAPI, PurchaseOrderAcknowledgeAPI
from .performance import calculate_average_response_time, \
    calculate_fulfillment_rate, calculate_on_time_delivery_rate, \
    calculate_quality_rating_avg


@receiver(PurchaseOrderDetailsAPI.purchase_order_updated, sender=PurchaseOrderBean)
def update_vendor_performance(sender, instance, created, **kwargs):
    """
    Signal handler to update VendorPerformanceBean when PurchaseOrderBean status is changed to 'Completed'.
    """
    request = kwargs.get("request")
    origin_data = kwargs.get("origin_data")
    updated_fulfillment_rate = None
    updated_delivery_rate = None
    updated_quality_rating = None
    updated_avg_response = None

    for key, val in request.data.items():

        """ Calculate performance of the vendor based on the purchase order status completion """

        if key == "status":
            updated_fulfillment_rate = calculate_fulfillment_rate(
                vendor_id=origin_data["vendor"], completed=(val.lower() == "completed"))

            if val.lower() == "completed":
                if request.data.get("delivery_date", None):
                    delivery_time = request.data.get("delivery_date")
                    delivery_time = datetime.datetime.strptime(delivery_time, "%Y-%m-%dT%H:%M:%S.%fZ").timestamp()
                else:
                    delivery_time = datetime.datetime.now().timestamp()

                delivered_on_time = True if (datetime.datetime.strptime(
                    origin_data["delivery_date"],"%Y-%m-%dT%H:%M:%S.%fZ").timestamp() - delivery_time) > 0 else False

                updated_delivery_rate = calculate_on_time_delivery_rate(
                    vendor_id=origin_data["vendor"], completed_on_time=delivered_on_time)

                quality_rating = request.data.get("quality_rating", None)
                updated_quality_rating = calculate_quality_rating_avg(
                    vendor_id=origin_data["vendor"], quality_rating=quality_rating)

        if key == "acknowledgement_date":
            updated_avg_response = calculate_average_response_time(
                origin_data=origin_data, purchase_order_request=request.data)

    if any([updated_avg_response, updated_fulfillment_rate, updated_delivery_rate, updated_quality_rating]):

        vendor_performance = VendorPerformanceBean.objects.filter(vendor_id=instance.vendor_id)

        """ Create performance entry for a specific vendor if not exist """

        if not vendor_performance:
            try:
                VendorPerformanceBean.objects.create(
                    vendor=VendorBean.objects.get(_id=origin_data.get("vendor")),
                    average_response_time=updated_avg_response if updated_avg_response else 0,
                    fulfillment_rate=updated_fulfillment_rate if updated_avg_response else 0,
                    on_time_delivery_rate=updated_delivery_rate if updated_avg_response else 0,
                    quality_rating_avg=updated_quality_rating)
                return Response(status=status.HTTP_201_CREATED)
            except:
                raise Exception("Failed - error occurred!")

        performance_serialized_data = VendorPerformanceSerializer(vendor_performance[0]).data

        updated_performance = {}

        """ Update performance metrics for a specific vendor """

        updated_performance["vendor"] = performance_serialized_data.get("vendor")
        updated_performance["average_response_time"] = \
            updated_avg_response if updated_avg_response else performance_serialized_data.get("average_response_time")
        updated_performance["fulfillment_rate"] = \
            updated_fulfillment_rate if updated_fulfillment_rate else performance_serialized_data.get(
                "fulfillment_rate")
        updated_performance["on_time_delivery_rate"] = \
            updated_delivery_rate if updated_delivery_rate else performance_serialized_data.get("on_time_delivery_rate")
        updated_performance["quality_rating_avg"] = \
            updated_quality_rating if updated_quality_rating else performance_serialized_data.get("quality_rating_avg")

        serializer = VendorPerformanceSerializer(
            VendorPerformanceBean.objects.get(vendor_id=instance.vendor_id), data=updated_performance)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@receiver(PurchaseOrderAcknowledgeAPI.purchase_order_updated, sender=PurchaseOrderBean)
def update_average_response_time(sender, instance, created, **kwargs):
    request = kwargs.get("request")
    origin_data = kwargs.get("origin_data")
    updated_avg_response = None

    updated_avg_response = calculate_average_response_time(
        origin_data=origin_data, purchase_order_request=request.data)

    vendor_performance = VendorPerformanceBean.objects.filter(vendor_id=instance.vendor_id)

    """ Create performance entry for a specific vendor if not exist """

    if not vendor_performance:
        try:
            VendorPerformanceBean.objects.create(
                vendor=VendorBean.objects.get(_id=origin_data.get("vendor")),
                average_response_time=updated_avg_response)
            return Response(status=status.HTTP_201_CREATED)
        except:
            raise Exception("Failed - error occured!!")

    performance_serialized_data = VendorPerformanceSerializer(vendor_performance[0]).data

    updated_performance = {}

    """ Update performance metrics for a specific vendor """

    updated_performance["vendor"] = performance_serialized_data.get("vendor")
    updated_performance["average_response_time"] = updated_avg_response
    updated_performance["fulfillment_rate"] = performance_serialized_data.get("fulfillment_rate")
    updated_performance["on_time_delivery_rate"] = performance_serialized_data.get("on_time_delivery_rate")
    updated_performance["quality_rating_avg"] = performance_serialized_data.get("quality_rating_avg")
    serializer = VendorPerformanceSerializer(
        VendorPerformanceBean.objects.get(vendor_id=instance.vendor_id), data=updated_performance)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
