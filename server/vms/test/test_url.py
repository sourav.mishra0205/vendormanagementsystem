from django.test import SimpleTestCase
from django.urls import resolve, reverse
from vms.api.vendor_api import VendorDetailsAPI, VendorAPI
from vms.api.vendor_performance_api import VendorPerformanceAPI
from vms.auth import CustomAuthToken
from vms.api.purchase_order_api import PurchaseOrderAPI, PurchaseOrderDetailsAPI, PurchaseOrderAcknowledgeAPI


class TestUrls(SimpleTestCase):

    def test_auth_url_resolves(self):
        print("Resolving URL: 'auth/'")
        url = reverse('auth')
        self.assertEquals(resolve(url).func.view_class, CustomAuthToken)
        print("PASSED - URL Resolved successfully!!")
        print("="*50, "\n")

    def test_vendors_url_resolves(self):
        print("Resolving URL: 'vendors/'")
        url = reverse('vendors')
        self.assertEquals(resolve(url).func.view_class, VendorAPI)
        print("PASSED - URL Resolved successfully!!")
        print("="*50, "\n")

    def test_vendor_details_url_resolves(self):
        print("Resolving URL: 'vendors/<vendor_id>/'")
        url = reverse('vendor_details', kwargs={'vendor_id': 0})
        self.assertEquals(resolve(url).func.view_class, VendorDetailsAPI)
        print("PASSED - URL Resolved successfully!!")
        print("="*50, "\n")
    
    def test_vendor_performance_url_resolves(self):
        print("Resolving URL: 'vendors/<vendor_id>/performance/'")
        url = reverse('vendor_performance', kwargs={'vendor_id': 0})
        self.assertEquals(resolve(url).func.view_class, VendorPerformanceAPI)
        print("PASSED - URL Resolved successfully!!")
        print("="*50, "\n")

    def test_purchase_orders_url_resolves(self):
        print("Resolving URL: 'purchase_orders/'")
        url = reverse('purchase_orders')
        self.assertEquals(resolve(url).func.view_class, PurchaseOrderAPI)
        print("PASSED - URL Resolved successfully!!")
        print("="*50, "\n")

    def test_purchase_order_details_url_resolves(self):
        print("Resolving URL: 'purchase_orders/<po_id>/'")
        url = reverse('purchase_order_details', kwargs={'po_id': 0})
        self.assertEquals(resolve(url).func.view_class, PurchaseOrderDetailsAPI)
        print("PASSED - URL Resolved successfully!!")
        print("="*50, "\n")

    def test_order_acknowledge_url_resolves(self):
        print("Resolving URL: 'purchase_orders/<po_id>/acknowledge/'")
        url = reverse('order_acknowledge', kwargs={'po_id': 0})
        self.assertEquals(resolve(url).func.view_class, PurchaseOrderAcknowledgeAPI)
        print("PASSED - URL Resolved successfully!!")
        print("="*50, "\n")
        