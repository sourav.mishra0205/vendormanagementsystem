from django.contrib.auth.models import User
from vms.models import VendorBean
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from .test_data import username

user = User.objects.get(username=username)
token = Token.objects.get(user__username=username)


class TestVendorAPI(APITestCase):
       
    
    def test_vendors_post_201(self):
        print("Test - VendorAPI - [POST] 201_CREATED")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("vendors")
        response = self.client.post(url, format='json', data={"name":"vendor1", "contact_details":"vendor1@mail.com", "address":"bangalore"})
        self.assertEquals(response.status_code, 201)
        self.assertEquals(VendorBean.objects.count(), 1)
        self.assertEquals(response.data,
        {
            "name":"vendor1",
            "contact_details":"vendor1@mail.com",
            "address":"bangalore"
        }
        )
        print("Test Execution Success!")
        print("="*50)
        pass

    def test_vendors_get_404(self):
        print("Test - VendorAPI - [GET] 404_NOT_FOUND")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("vendors")
        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, 404)
        print("Test Execution Success!")
        print("="*50)
    
    def test_vendors_get_200(self):
        print("Test - VendorAPI - [GET] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("vendors")
        VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        VendorBean.objects.create(name="vendor2", contact_details="vendor2@mail.com", address="BLR")
        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(VendorBean.objects.count(), 2)
        print("Test Execution Success!")
        print("="*50)

    def test_vendor_details_get_200(self):
        print("Test - VendorDetailsAPI - [GET] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("vendor_details", kwargs={"vendor_id": 1})
        VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, 200)
        print("Test Execution Success!")
        print("="*50)

    def test_vendor_details_delete_200(self):
        print("Test - VendorDetailsAPI - [DELETE] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("vendor_details", kwargs={"vendor_id": 1})
        VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        response = self.client.delete(url, format='json')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(VendorBean.objects.count(), 0)
        print("Test Execution Success!")
        print("="*50)

    def test_vendor_details_put_200(self):
        print("Test - VendorDetailsAPI - [PUT] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("vendor_details", kwargs={"vendor_id": 1})
        VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        response = self.client.put(url, format='json', data={"address":"BBSR"})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data, 
        {   '_id': 1, 
            'name': 'vendor1',
            'contact_details':
            'vendor1@mail.com',
            'address': 'BBSR'})
        print("Test Execution Success!")
        print("="*50)
    
    