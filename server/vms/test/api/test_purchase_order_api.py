from django.contrib.auth.models import User
from vms.models import VendorBean, PurchaseOrderBean
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from .test_data import username

user = User.objects.get(username=username)
token = Token.objects.get(user__username=username)


class TestPurchaseOrderAPI(APITestCase):
       
    def test_purchase_order_post_201(self):
        print("Test - PurchaseOrder - [POST] 201_CREATED")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("purchase_orders")
        vendor_obj = VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        response = self.client.post(url, format='json',
        data= {
            "order_date": "2024-05-05T10:30:00Z",
            "delivery_date": "2024-05-06T12:32:35.081334Z",
            "items": {
                "name": "iPhone14Pro",
                "color": "black",
                "spec": {
                    "ROM": "128GB",
                    "camera": "18MP"
                }
            },
            "quantity": 1,
            "status": "Pending",
            "quality_rating": None,
            "issue_date": "2024-05-05T10:35:00Z",
            "acknowledgement_date": None,
            "vendor": vendor_obj._id
            })
        self.assertEquals(response.status_code, 201)
        self.assertEquals(PurchaseOrderBean.objects.count(), 1)
        print("Test Execution Success!")
        print("="*50)
        pass

    def test_purchase_orders_get_404(self):
        print("Test - PurchaseOrderAPI - [GET] 404_NOT_FOUND")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("purchase_orders")
        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, 404)
        print("Test Execution Success!")
        print("="*50)
    
    def test_purchase_orers_get_200(self):
        print("Test - PurchaseOrderAPI - [GET] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("purchase_orders")
        vendor_obj = VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        PurchaseOrderBean.objects.create(
                order_date = "2024-05-05T10:30:00Z",
                delivery_date = "2024-05-06T12:32:35.081334Z",
                items = {
                    "name": "iPhone14Pro",
                    "color": "black",
                    "spec": {
                        "ROM": "128GB",
                        "camera": "18MP"
                    }
                },
                quantity = 1,
                status = "Pending",
                quality_rating = None,
                issue_date = "2024-05-05T10:35:00Z",
                acknowledgement_date = None,
                vendor = VendorBean.objects.get(_id=vendor_obj._id)
        )
        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(PurchaseOrderBean.objects.count(), 1)
        print("Test Execution Success!")
        print("="*50)
    
    def test_purchase_order_details_delete_200(self):
        print("Test - PurchaseOrderDetailsAPI - [DELETE] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("purchase_order_details", kwargs={"po_id": 1})
        vendor_obj = VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        PurchaseOrderBean.objects.create(
                order_date = "2024-05-05T10:30:00Z",
                delivery_date = "2024-05-06T12:32:35.081334Z",
                items = {
                    "name": "iPhone14Pro",
                    "color": "black",
                    "spec": {
                        "ROM": "128GB",
                        "camera": "18MP"
                    }
                },
                quantity = 1,
                status = "Pending",
                quality_rating = None,
                issue_date = "2024-05-05T10:35:00Z",
                acknowledgement_date = None,
                vendor = VendorBean.objects.get(_id=vendor_obj._id)
        )
        response = self.client.delete(url, format='json')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(PurchaseOrderBean.objects.count(), 0)
        print("Test Execution Success!")
        print("="*50)

    def test_purchase_order_details_get_200(self):
        print("Test - PurchaseOrderDetailsAPI - [GET] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("purchase_order_details", kwargs={"po_id": 1})
        vendor_obj = VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        PurchaseOrderBean.objects.create(
                order_date = "2024-05-05T10:30:00Z",
                delivery_date = "2024-05-06T12:32:35.081334Z",
                items = {
                    "name": "iPhone14Pro",
                    "color": "black",
                    "spec": {
                        "ROM": "128GB",
                        "camera": "18MP"
                    }
                },
                quantity = 1,
                status = "Pending",
                quality_rating = None,
                issue_date = "2024-05-05T10:35:00Z",
                acknowledgement_date = None,
                vendor = VendorBean.objects.get(_id=vendor_obj._id)
        )
        response = self.client.get(url, format='json')
        self.assertEquals(response.status_code, 200)
        print("Test Execution Success!")
        print("="*50)

    def test_purchase_order_details_put_200(self):
        print("Test - PurchaseOrderDetailsAPI - [PUT] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("purchase_order_details", kwargs={"po_id": 1})
        vendor_obj = VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        PurchaseOrderBean.objects.create(
                order_date = "2024-05-05T10:30:00Z",
                delivery_date = "2024-05-06T12:32:35.081334Z",
                items = {
                    "name": "iPhone14Pro",
                    "color": "black",
                    "spec": {
                        "ROM": "128GB",
                        "camera": "18MP"
                    }
                },
                quantity = 1,
                status = "Pending",
                quality_rating = None,
                issue_date = "2024-05-05T10:35:00Z",
                acknowledgement_date = "2024-05-07T22:51:19.918912Z",
                vendor = VendorBean.objects.get(_id=vendor_obj._id)
        )
        response = self.client.put(url, format='json', data={"status":"Completed", "quality_rating": 4.6})
        self.assertEquals(response.status_code, 200)   
        print("Test Execution Success!")
        print("="*50)

    def test_purchase_order_acknowledge_put_200(self):
        print("Test - PurchaseOrderAcknowledgeAPI - [PUT] 200_OK")
        self.client.force_authenticate(token=token, user=user)
        url = reverse("order_acknowledge", kwargs={"po_id": 1})
        vendor_obj = VendorBean.objects.create(name="vendor1", contact_details="vendor1@mail.com", address="BLR")
        PurchaseOrderBean.objects.create(
                order_date = "2024-05-05T10:30:00Z",
                delivery_date = "2024-05-06T12:32:35.081334Z",
                items = {
                    "name": "iPhone14Pro",
                    "color": "black",
                    "spec": {
                        "ROM": "128GB",
                        "camera": "18MP"
                    }
                },
                quantity = 1,
                status = "Pending",
                quality_rating = None,
                issue_date = "2024-05-05T10:35:00Z",
                acknowledgement_date = None,
                vendor = VendorBean.objects.get(_id=vendor_obj._id)
        )
        response = self.client.put(url, format='json', data={})
        self.assertEquals(response.status_code, 200)
        print("Test Execution Success!")
        print("="*50)