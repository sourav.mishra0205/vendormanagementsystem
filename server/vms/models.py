from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class VendorBean(models.Model):
    _id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150)
    contact_details = models.TextField()
    address = models.TextField()

    def __str__(self) -> str:
        return f"{self._id}:{self.name}"


class PurchaseOrderBean(models.Model):
    _id = models.AutoField(primary_key=True)
    vendor = models.ForeignKey(VendorBean, on_delete=models.CASCADE)
    order_date = models.DateTimeField()
    delivery_date = models.DateTimeField()
    items = models.JSONField()
    quantity = models.IntegerField()
    status = models.CharField(max_length=20)
    quality_rating = models.FloatField(null=True)
    issue_date = models.DateTimeField()
    acknowledgement_date = models.DateTimeField(null=True)
    pass


class VendorPerformanceBean(models.Model):
    _id = models.AutoField(primary_key=True)
    vendor = models.ForeignKey(VendorBean, on_delete=models.CASCADE)
    on_time_delivery_rate = models.FloatField(default=0)
    quality_rating_avg = models.FloatField(null=True)
    average_response_time = models.FloatField(default=0)
    fulfillment_rate = models.FloatField(default=0)
    pass
