from rest_framework import status
from rest_framework import permissions, authentication
from rest_framework.views import APIView
from rest_framework.response import Response
from ..serializers import VendorSerializer
from ..models import VendorBean


class VendorAPI(APIView):
    serializer_class = VendorSerializer
    authentication_classes = [
        authentication.TokenAuthentication
    ]
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request):
        vendors_list = []
        try:
            vendors = VendorBean.objects.all()
            if not vendors:
                return Response(
                    {"status": "Failed", "message": f"No Data Available - {vendors_list}"},
                    status=status.HTTP_404_NOT_FOUND)
            for vendor in vendors:
                vendor_data = self.serializer_class(vendor).data
                vendors_list.append(vendor_data)
            return Response(vendors_list, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                {"status": "Failed", "message": ["Error While Processing : " + str(e)]},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        """
        Create DB Entry for the Vendor Model

        Args:
            request (http.request): HTTP request received.

        Returns:
            HTTPS Response: API response with success or failure.

        """
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            serialized_data = serializer.data
            try:
                VendorBean.objects.create(
                    name=serialized_data.get("name"),
                    contact_details=serialized_data.get("contact_details"),
                    address=serialized_data.get("address"))
                return Response(serialized_data, status=status.HTTP_201_CREATED)
            except Exception as e:
                return Response(serialized_data, status=status.HTTP_417_EXPECTATION_FAILED)
        except Exception as e:
            return Response(
                {"status": "Failed", "message": "Error While Processing : " + str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class VendorDetailsAPI(APIView):
    authentication_classes = [
        authentication.TokenAuthentication
    ]
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request, vendor_id: str = ""):
        try:
            vendor_data = VendorBean.objects.get(_id=vendor_id)
            serializer = VendorSerializer(vendor_data)
            return Response(serializer.data)
        except VendorBean.DoesNotExist:
            return Response({"status": "Failed", "message": f"Vendor with ID {vendor_id} not exist"},
                            status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, vendor_id: str = ""):
        try:
            try:
                vendor_obj = VendorBean.objects.get(_id=vendor_id)
            except VendorBean.DoesNotExist:
                return Response({"status": "Failed", "message": f"Vendor Object Not Found with ID- {vendor_id}"},
                                status=status.HTTP_404_NOT_FOUND)
            vendor_obj.delete()
            return Response({"status": "Success", "message": "Object Deleted Successfully."},
                            status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"status": "Failed", "message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request, vendor_id: str = ""):
        origin_data = {}
        try:
            try:
                vendor_obj = VendorBean.objects.get(_id=vendor_id)
                serialized_data = VendorSerializer(vendor_obj).data
            except VendorBean.DoesNotExist:
                return Response({"status": "Failed", "message": f"Vendor Object Not Found with ID- {vendor_id}"},
                                status=status.HTTP_404_NOT_FOUND)

            origin_data["name"] = serialized_data.get("name")
            origin_data["contact_details"] = serialized_data.get("contact_details")
            origin_data["address"] = serialized_data.get("address")

            for key, val in request.data.items():
                if hasattr(vendor_obj, key):
                    origin_data[key] = val
                else:
                    return Response({"status": "Failed", "message": f"Invalid Payload key - {key}"},
                                    status=status.HTTP_400_BAD_REQUEST)

            serializer = VendorSerializer(vendor_obj, data=origin_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({"status": "Failed", "message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
