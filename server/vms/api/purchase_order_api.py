import datetime
from django.dispatch import Signal
from rest_framework import status
from rest_framework import permissions, authentication
from rest_framework.response import Response
from rest_framework.views import APIView
from ..serializers import PurchaseOrderSerializer
from ..models import PurchaseOrderBean, VendorBean


class PurchaseOrderAPI(APIView):
    serializer_class = PurchaseOrderSerializer

    authentication_classes = [
        authentication.TokenAuthentication
    ]
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request):
        po_list = []
        try:
            purchase_orders = PurchaseOrderBean.objects.all()
            if not purchase_orders:
                return Response(
                    {"status": "Failed", "message": f"No Data Available - {po_list}"},
                    status=status.HTTP_404_NOT_FOUND)
            if request.query_params.get("vendor_id", None):
                for order in purchase_orders:
                    if str(order.vendor_id) == request.query_params.get("vendor_id"):
                        serialized_data = PurchaseOrderSerializer(order).data
                        po_list.append(serialized_data)
            else:
                for order in purchase_orders:
                    serialized_data = PurchaseOrderSerializer(order).data
                    po_list.append(serialized_data)
            return Response(po_list)
        except Exception as e:
            return Response(
                {"status": "Failed", "message": ["Error While Processing : " + str(e)]},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request):
        """
        Create DB Entry for the PurchaseOrderBean Model

        Args:
            request (http.request): HTTP request received.

        Returns:
            HTTPS Response: API response with success or failure.

        """
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=False)
            serialized_data = serializer.data
            try:
                PurchaseOrderBean.objects.create(
                    order_date=serialized_data.get("order_date"),
                    delivery_date=serialized_data.get("delivery_date"),
                    items=serialized_data.get("items"),
                    quantity=serialized_data.get("quantity"),
                    status=serialized_data.get("status"),
                    quality_rating=serialized_data.get("quality_rating"),
                    issue_date=serialized_data.get("issue_date"),
                    acknowledgement_date=serialized_data.get("acknowledgement_date"),
                    vendor=VendorBean.objects.get(_id=serialized_data.get("vendor"))
                )
                return Response(serialized_data, status=status.HTTP_201_CREATED)
            except Exception as e:
                return Response({"status": "Failed", "message": str(e)}, status=status.HTTP_417_EXPECTATION_FAILED)
        except Exception as e:
            return Response(
                {"status": "Failed", "message": "Error While Processing : " + str(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PurchaseOrderDetailsAPI(APIView):
    authentication_classes = [
        authentication.TokenAuthentication
    ]
    permission_classes = [
        permissions.IsAuthenticated
    ]

    purchase_order_updated = Signal()
    purchase_order_updated.providing_args = ["request", "origin_data"]

    def get(self, request, po_id: str = ""):
        try:
            po_data = PurchaseOrderBean.objects.get(_id=po_id)
            serializer = PurchaseOrderSerializer(po_data)
            return Response(serializer.data)
        except PurchaseOrderBean.DoesNotExist:
            return Response({"status": "Failed", "message": f"Purchase Order with ID {po_id} not exist"},
                            status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, po_id: str = ""):
        try:
            try:
                po_obj = PurchaseOrderBean.objects.get(_id=po_id)
            except PurchaseOrderBean.DoesNotExist:
                return Response({"status": "Failed", "message": f"Purchase Order Not Found with ID- {po_id}"},
                                status=status.HTTP_404_NOT_FOUND)
            po_obj.delete()
            return Response({"status": "Success", "message": "Order Deleted Successfully."},
                            status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"status": "Failed", "message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request, po_id: str = ""):
        origin_data = {}
        check_delivery_date = False
        try:
            try:
                po_obj = PurchaseOrderBean.objects.get(_id=po_id)
                serialized_data = PurchaseOrderSerializer(po_obj).data

            except PurchaseOrderBean.DoesNotExist:
                return Response({"status": "Failed", "message": f"Purchase Order Not Found with ID- {po_id}"},
                                status=status.HTTP_404_NOT_FOUND)

            origin_data["order_date"] = serialized_data.get("order_date")
            origin_data["delivery_date"] = serialized_data.get("delivery_date")
            origin_data["items"] = serialized_data.get("items")
            origin_data["quantity"] = serialized_data.get("quantity")
            origin_data["status"] = serialized_data.get("status")
            origin_data["quality_rating"] = serialized_data.get("quality_rating")
            origin_data["issue_date"] = serialized_data.get("issue_date")
            origin_data["acknowledgement_date"] = serialized_data.get("acknowledgement_date")
            origin_data["vendor"] = serialized_data.get("vendor")

            for key, val in request.data.items():

                if key == "acknowledgement_date":
                    if origin_data.get("acknowledgement_date"):
                        return Response(
                            {"status": "Failed",
                             "message": f"Already acknowledged on {origin_data.get('acknowledgement_date')},"
                                        f" rewrite not allowed"},
                            status=status.HTTP_406_NOT_ACCEPTABLE)

                if key == "status":
                    if val == origin_data.get("status"):
                        return Response(
                            {"status": "Failed", "message": f"Product status can't be updated from {val} to {val}"},
                            status=status.HTTP_406_NOT_ACCEPTABLE)

                    elif val.lower() == "completed":
                        check_delivery_date = True
                        if not origin_data.get("acknowledgement_date", None):
                            return Response(
                                {"status": "Failed", "message": f"acknowledgement_date to be updated prior to status"},
                                status=status.HTTP_406_NOT_ACCEPTABLE)

                if hasattr(po_obj, key):
                    origin_data[key] = val
                else:
                    return Response({"status": "Failed", "message": f"Invalid Payload key - {key}"},
                                    status=status.HTTP_400_BAD_REQUEST)

            self.purchase_order_updated.send(sender=PurchaseOrderBean, instance=po_obj, created=False, request=request,
                                             origin_data=origin_data)

            if check_delivery_date:
                if not request.data.get("delivery_date", None):
                    origin_data["delivery_date"] = datetime.datetime.now()

            serializer = PurchaseOrderSerializer(po_obj, data=origin_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({"status": "Failed", "message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PurchaseOrderAcknowledgeAPI(APIView):
    authentication_classes = [
        authentication.TokenAuthentication
    ]
    permission_classes = [
        permissions.IsAuthenticated
    ]

    purchase_order_updated = Signal()
    purchase_order_updated.providing_args = ["request", "origin_data"]

    def put(self, request, po_id: str):
        origin_data = {}
        try:
            try:
                po_obj = PurchaseOrderBean.objects.get(_id=po_id)
                serialized_data = PurchaseOrderSerializer(po_obj).data

            except PurchaseOrderBean.DoesNotExist:
                return Response({"status": "Failed", "message": f"Purchase Order Not Found with ID- {po_id}"},
                                status=status.HTTP_404_NOT_FOUND)

            origin_data["order_date"] = serialized_data.get("order_date")
            origin_data["delivery_date"] = serialized_data.get("delivery_date")
            origin_data["items"] = serialized_data.get("items")
            origin_data["quantity"] = serialized_data.get("quantity")
            origin_data["status"] = serialized_data.get("status")
            origin_data["quality_rating"] = serialized_data.get("quality_rating")
            origin_data["issue_date"] = serialized_data.get("issue_date")
            if serialized_data.get("acknowledgement_date"):
                return Response(
                    {"status": "Failed",
                     "message": f"Already acknowledged on {origin_data.get('acknowledgement_date')},"
                                f" rewrite not allowed"},
                    status=status.HTTP_406_NOT_ACCEPTABLE)
            origin_data["acknowledgement_date"] = datetime.datetime.now()
            origin_data["vendor"] = serialized_data.get("vendor")
            request.data["acknowledgement_date"] = origin_data["acknowledgement_date"]

            serializer = PurchaseOrderSerializer(po_obj, data=origin_data)
            self.purchase_order_updated.send(sender=PurchaseOrderBean, instance=po_obj, created=False, request=request,
                                             origin_data=origin_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({"status": "Failed", "message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
