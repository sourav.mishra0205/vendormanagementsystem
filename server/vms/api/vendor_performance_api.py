from rest_framework import status
from rest_framework import permissions, authentication
from rest_framework.views import APIView
from rest_framework.response import Response
from ..serializers import VendorPerformanceSerializer
from ..models import VendorPerformanceBean


class VendorPerformanceAPI(APIView):

    serializer_class = VendorPerformanceSerializer

    authentication_classes = [
        authentication.TokenAuthentication
    ]
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request, vendor_id: str = ""):
        try:
            performance_data = VendorPerformanceBean.objects.get(vendor_id=vendor_id)
            if not performance_data:
                return Response(
                    {"status": "Failed", "message": f"No Data Available - {performance_data}"},
                    status=status.HTTP_404_NOT_FOUND)
            serialized_data = self.serializer_class(performance_data).data
            return Response(serialized_data, status=status.HTTP_200_OK)
        except VendorPerformanceBean.DoesNotExist:
            return Response({"status": "Failed", "message": f"Performance of Vendor with ID {vendor_id} not exist"},
                            status=status.HTTP_404_NOT_FOUND)
