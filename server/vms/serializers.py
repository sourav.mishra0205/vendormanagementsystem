from rest_framework import serializers
from .models import *


class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendorBean
        fields = "__all__"
    pass


class PurchaseOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = PurchaseOrderBean
        fields = "__all__"
    pass


class VendorPerformanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = VendorPerformanceBean
        fields = "__all__"
    pass
