# Vendor Management System



## Table of Contents
  -  [Prerequisites](#prerequisites)
  -  [Installation](#installation)
  -  [Usage](#usage)
  -  [TestSuite](#testsuite)
  -  [Contact](#contact)


## Prerequisites

* Install Python3.10.8 or Higher

## Installation
1. Clone the repository
    ```sh
    git clone https://gitlab.com/sourav.mishra0205/vendormanagementsystem.git
    ```
2. Navigate to the project & Open a terminal, install virtualenv
    ```sh
    python -m venv venv
    ```
    Update Python interpreter path
3. Activate the virtualenv
    
    For Windows:
   ```sh
   venv\Scripts\activate
   ```
    For Linux:
   ```
   source venv/bin/activate
   ```
4. Updgrade python pip version
    ```sh
    python -m pip install --upgrade pip
    ```
5. Install python packages
   ```sh
   cd ./VendorManagementSystem/server/ && pip install -r requirements.txt
   ```
6. Make Migrations
    ```sh
    python manage.py makemigrations
    ```
7. Migrate
    ```sh
    python manage.py migrate
    ```
8. Create superuser
    ```sh
    python manage.py createsuperuser
    ```
9. Start django server
   ```sh
   python manage.py runserver
   ```


## Usage
API Authentication:
1. This project requires authentication to access most of its endpoints. So Initial step would be to get authentication token with the help of superuser & its password.
    
    Using Django manage.py command:
    ```sh
    python ./manage.py drf_create_token <username>
    ```
    Using REST API:
    ```sh
    URL: /api/auth/
    METHOD: POST
    REQUEST BODY: {"username": <username>, "password": <password>}
    ```
    Expected Token:
    ```sh
    Generated token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b for user user1
    ```
    Authoriation Token = token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b(sample token), will be used across all apis.

2. Endpoint: vendors
    ```sh
    URL: /api/vendors/
    ```
    ```sh
    DESCRIPTION: Retrieves all vendor information.
    METHOD: GET
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    STATUS: 200 OK
    RESPONSE BODY: [
        {
            "_id": 1,
            "name": "Vendor1",
            "contact_details": "vendor1@gmail.com",
            "address": "Bangalore"
        },
        {
            "_id": 2,
            "name": "Vendor2",
            "contact_details": "vendor2@gmail.com",
            "address": "Bhubaneswar"
        }
    ]
    ```
    ```sh
    DESCRIPTION: Create a vendor and add it in the DB.
    METHOD: POST
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    REQUEST BODY: {
        "name":"vendor1",
        "contact_details":"sourav.mishra0205@gmail.com",
        "address":"Bangalore"
    }
    STATUS: 201 CREATED
    RESPONSE BODY: {
        "_id":"<id>"
        "name":"vendor1",
        "contact_details":"sourav.mishra0205@gmail.com",
        "address":"Bangalore"
    }
    ```
    ```sh
    URL: /api/vendors/<vendor_id>/
    ```
    ```sh
    DESCRIPTION: Retrieves a vendor details.
    METHOD: GET
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    STATUS: 200 OK
    RESPONSE BODY: {
        "_id": <vendor_id>,
        "name": "Vendor1",
        "contact_details": "vendor1@gmail.com",
        "address": "Bangalore"
    }
    ```
    ```sh
    DESCRIPTION: Delete a vendor from DB.
    METHOD: DELETE
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    STATUS: 200 OK
    RESPONSE BODY: {
        "status": "Success",
        "message": "Object Deleted Successfully."
    }
    ```
    ```sh
    DESCRIPTION: Update a specific vendor details.
    METHOD: PUT
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    REQUEST BODY: {
        "contact_details": "vendor12@gmail.com"
    }
    STATUS: 200 OK
    RESPONSE BODY: {
        "_id": <vendor_id>,
        "name": "Vendor1",
        "contact_details": "vendor12@gmail.com",
        "address": "Bangalore"
    }
    ```
    ```sh
    URL: /api/vendors/<vendor_id>/performance/
    ```
    ```sh
    DESCRIPTION: Retrive a specific vendor performance.
    METHOD: GET
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    STATUS: 200 OK
    RESPONSE BODY: {
    "_id": <id>,
    "on_time_delivery_rate": 1,
    "quality_rating_avg": 4.5,
    "average_response_time": 130.4 # In seconds
    "fulfillment_rate": 1,
    "vendor": <vendor_id>
    }
    ```
3. Endpoint: purchase_orders
    ```sh
    URL: /api/purchase_orders/
    ```
    ```sh
    DESCRIPTION: Retrieves all purchase order information.
    METHOD: GET
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    STATUS: 200 OK
    RESPONSE BODY: [
        {
            "_id": <id>,
            "order_date": "2024-05-05T10:30:00Z",
            "delivery_date": "2024-05-06T12:32:35.081334Z",
            "items": {
            "name": "iPhone14Pro",
            "color": "black",
            "spec": {
                "ROM": "128GB",
                "camera": "18MP"
            }
            },
            "quantity": 2,
            "status": "Completed",
            "quality_rating": 4.7,
            "issue_date": "2024-05-05T10:35:00Z",
            "acknowledgement_date": "2024-05-05T18:00:19.686358Z",
            "vendor": <vendor_id>
        }
    ]
    ```
    ```sh
    DESCRIPTION: Create a purchase order and stores in DB.
    METHOD: POST
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    REQUEST BODY: [
        {
            "order_date": "2024-05-05T10:30:00Z",
            "delivery_date": "2024-05-06T12:32:35.081334Z",
            "items": {
                <items>
            },
            "quantity": <quantity>,
            "status": <status>,
            "quality_rating": <rating>,    # Can be null
            "issue_date": "2024-05-05T10:35:00Z",
            "acknowledgement_date": <ack_date>,  # Can be null
            "vendor": <vendor_id>
        }
    ]
    STATUS: 200 OK
    ```
    ```sh
    URL: /api/purchase_orders/<po_id>/
    ```
    ```sh
    DESCRIPTION: allowed to view, delete, update a purchase order request.
    ALLOWED METHODS: [GET, DELETE, PUT]
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    PUT REQUEST BODY: {
        "status":"Completed",
        "quality_rating": 4.5
    }
    STATUS: 200 OK
    ```
    ```sh
    URL: /api/purchase_orders/<po_id>/acknowledge/
    ```
    ```sh
    DESCRIPTION: method allowed to acknowledge a purchase request by vendor.
    ALLOWED METHODS: [PUT]
    HEADERS: {"Authorization": "token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}
    PUT REQUEST BODY: {}  # It takes empty payload. and updates the ack date.
    STATUS: 200 OK
    RESPONSE BODY: {
        "_id": <id>
        "order_date": "2024-05-05T10:30:00Z",
        "delivery_date": "2024-05-06T12:32:35.081334Z",
        "items": {
            <items>
        },
        "quantity": <quantity>,
        "status": <status>,
        "quality_rating": <rating>,    # Can be null
        "issue_date": "2024-05-05T10:35:00Z",
        "acknowledgement_date": "2024-05-05T10:40:00Z",  # Updated
        "vendor": <vendor_id>
    }
    ```

    


## TestSuite
1. Give username of the superuser in the python file.
    ```sh
        \VendorManagementSystem\server\vms\test\api\test_data.py
    ```

2. Run the command in order to execute all testcases.

    ```sh
        python ./manage.py test
    ```

## Contact
    Email: sourav.mishra0205@gmail.com
    LinkedIn: https://www.linkedin.com/in/sourav-mishra-042762206/
